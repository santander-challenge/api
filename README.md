# beer-challenge-api

API REST desarrollada en Node.JS para el beer-meetup challenge.

### About

- API REST escrita en TypeScript. Diseñada para trabajar con MongoDB. Base de datos alojada en [atlas](https://www.mongodb.com/cloud/atlas).
- Documentación desarrollada con [swagger](https://www.npmjs.com/package/swagger-ui-express)
- Seguridad implementada con [auth0](https://auth0.com/)
- Test realizados con [mochajs](https://mochajs.org/) y [chaijs](https://www.chaijs.com/)
- Clima consultado en [https://community-open-weather-map.p.rapidapi.com](https://community-open-weather-map.p.rapidapi.com/)

### Supuestos

- La idea es que la API sea deployada en un ambiente linux + docker. Ver el apartado de "Running in production"
- Para la consulta del clima se asumió que todas las meetups se realizarán en Buenos Aires.
- Se realizaron test con covertura total pero "superficial"; es decir, no se terminaron de probar todas las posibilidades de si para X no puede pasar Y, etc.
- Los test se realizan sobre una base de datos igual pero no la misma que la "productiva". Productiva: beer_db, Test: beer_db_test.
- No usar la base de tests como persistencia ya que se borran todos los datos en cada run de test.

### Features

- Como usuario o admin quiero poder recibir notificaciones para estar al tanto de las meetups
- Como admin quiero armar una meetup para poder invitar otras personas
- Como usuario quiero inscribirme en una meetup para poder asistir
- Como usuario quiero hacer check-in en una meetup para poder avisar que estuve allí
- Como admin quiero saber cuantas cajas de birras tengo que comprar para poder provisionar el meetup
- Como admin y usuario quiero ver qué temperatura va a hacer el día de la meetup para saber si hace calor o no

### Install

```
git clone https://gitlab.com/santander-challenge/api
npm install
```

### Run

```
npm run webpack
```

in other bash session:

```
npm start
```

Checkear las variables de entorno

### Testing

To run the tests:

```
npm test
```

### Help

- [Swagger docs](http://localhost:3000/api-docs)
- [Requests de postman](https://gitlab.com/santander-challenge/api/blob/master/santander-challange.postman_collection.json)

### Running in production

Al mergear a branch master se inicia proceso de despliegue. Está orientado a trabajar con Docker en un servidor linux. Ver:

- [Dockerfile](https://gitlab.com/santander-challenge/api/blob/master/Dockerfile)
- [gitlab-ci.yml](https://gitlab.com/santander-challenge/api/blob/master/.gitlab-ci.yml)

### Author

- [Rodrigo Calvo](https://gitlab.com/recalvo)
- [Consultas](mailto:rodrigocalvo95@gmail.com)
