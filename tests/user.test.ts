import { expect } from "chai";
import * as UserService from "./../src/user/user.service";
import { User, UserModel } from "./../src/user/user.class";

let working_user_id: string = "";

describe("UserService", () => {
    before((done) => {
        require("./test_helper");
        const user: any = {
            "isEnabled": true,
            "isAdmin": true,
            "firstName": "Rodrigo",
            "lastName": "Test",
            "username": "rodrigo.calvo.prueba.users." + new Date().getTime() + "@gmail.com",
            "password": "asd1234"
        };

        UserService.create(user).then((u: any) => {
            working_user_id = u._id;
            done();
        });
    });
    describe("add", () => {
        it("should work", (done) => {
            const user: any = {
                "isEnabled": true,
                "isAdmin": true,
                "firstName": "Rodrigo",
                "lastName": "Test",
                "username": "rodrigo.calvo.prueba.users." + new Date().getTime() + "@gmail.com",
                "password": "asd1234"
            };
            UserService.create(user).then((u: any) => {
                expect(u).to.have.property("_id");
                done();
            });
        });
        it("should not work - repeat username", (done) => {
            const user: any = {
                "isEnabled": true,
                "isAdmin": true,
                "firstName": "Rodrigo",
                "lastName": "Test",
                "username": "rodrigo.calvo.prueba.users." + new Date().getTime() + "@gmail.com",
                "password": "asd1234"
            };
            UserService.create(user).then((u: any) => {
                expect(u).to.have.property("_id");

                UserService.create(user).then((_u: any) => {
                    expect(_u).to.not.have.property("_id");
                    done();
                }).catch((err: any) => {
                    expect(err).to.not.equals(undefined);
                    done();
                });
            });
        });
    });
    it("findAll", (done) => {
        UserService.findAll().then((users: Array<User>) => {
            expect(users).to.be.a("Array");
            done();
        });
    });
    it("find", (done) => {
        UserService.find(working_user_id).then((user: User) => {
            expect(user.firstName).to.equal("Rodrigo");
            expect(user.lastName).to.not.equal("Fernandez");
            done();
        });
    });
    it("update", (done) => {
        UserService.find(working_user_id).then((user: User) => {
            expect(user.firstName).to.equal("Rodrigo");

            user.firstName = "cambiado";
            UserService.update(working_user_id, user).then((_user: User) => {
                expect(_user.firstName).to.equal("cambiado");

                _user.firstName = "Rodrigo";
                UserService.update(working_user_id, _user).then((__user: User) => {
                    expect(__user.firstName).to.equal("Rodrigo");
                    done();
                });
            });
        });
    });
    it("remove", (done) => {
        UserService.remove(working_user_id).then((_u: any) => {
            expect(_u).to.be.a("undefined");
            done();
        });
    });
});