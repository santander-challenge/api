import { expect, should } from "chai";
import * as MeetupService from "./../src/meetup/meetup.service";
import { Meetup, MeetupModel } from "./../src/meetup/meetup.class";
import * as UserService from "./../src/user/user.service";
import * as InvitationService from "./../src/invitation/invitation.service";
import { User } from "./../src/user/user.class";
import { Invitation } from "./../src/invitation/invitation.class";


function addDays(date: Date, days: number): Date {
    var result: Date = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

let working_user_id: string = "";
let working_meetup_id: string = "";
let working_invitation_id: string = "";

describe("MeetupService", () => {
    before((done) => {
        require("./test_helper");
        const user: any = {
            "isEnabled": true,
            "isAdmin": true,
            "firstName": "Rodrigo",
            "lastName": "Test",
            "username": "rodrigo.calvo.prueba.meetups." + new Date().getTime() + "@gmail.com",
            "password": "asd1234"
        };

        const meetup: any = {
            date: addDays(new Date(), 5),
            description: "meetup de prueba meetups"
        };

        UserService.create(user).then((u: any) => {
            working_user_id = u._id;
            meetup.creator = working_user_id;

            MeetupService.create(meetup).then((m: any) => {
                working_meetup_id = m._id;

                const invitation: any = {
                    from: working_user_id,
                    to: working_user_id,
                    meetup: working_meetup_id
                };

                InvitationService.create(invitation).then((i: any) => {
                    working_invitation_id = i._id;
                    done();
                });
            });
        });
    });
    it("create", (done) => {
        const meetup: any = {
            creator: working_user_id,
            date: addDays(new Date(), 5),
            description: "meetup de prueba meetups 2"
        };

        MeetupService.create(meetup).then((m: any) => {
            expect(m).to.have.property("_id");
            done();
        });
    });
    it("findAll", (done) => {
        MeetupService.findAll().then((m: any) => {
            expect(m).to.be.a("Array");
            done();
        });
    });
    it("find", (done) => {
        MeetupService.find(working_meetup_id).then((_m: any) => {
            expect(_m).to.have.property("_id");
            done();
        });
    });
    it("findPopulated", (done) => {
        MeetupService.findPopulated(working_meetup_id).then((_m: any) => {
            expect(_m).to.have.property("_id");
            expect(_m.creator).to.have.property("_id");
            done();
        });
    });
    it("update", (done) => {
        MeetupService.find(working_meetup_id).then((m: any) => {
            expect(m).to.have.property("_id");
            m.description = "nueva descripción";

            MeetupService.update(m._id, m).then((_m: any) => {
                expect(_m).to.have.property("_id");
                expect(_m.description).to.equal("nueva descripción");

                MeetupService.find(_m._id).then((__m: any) => {
                    expect(__m).to.have.property("_id");
                    expect(__m.description).to.equal("nueva descripción");

                    __m.description = "meetup de prueba";
                    MeetupService.update(__m._id, __m).then((___m: any) => {
                        expect(___m).to.have.property("_id");
                        expect(___m.description).to.equal("meetup de prueba");

                        MeetupService.find(___m._id).then((____m: any) => {
                            expect(____m).to.have.property("_id");
                            expect(____m.description).to.equal("meetup de prueba");

                            done();
                        });
                    });
                });
            });
        });
    });
    it("userRegistered - false", (done) => {
        UserService.find(working_user_id).then((user: User) => {
            MeetupService.userRegistered(working_meetup_id, user).then((_m: boolean) => {
                expect(_m).to.equal(false);
                done();
            });
        });
    });
    it("register", (done) => {
        MeetupService.register(working_meetup_id, working_user_id).then((_m: void) => {
            expect(_m).to.equal(undefined);
            done();
        });
    });
    it("userRegistered - true", (done) => {
        UserService.find(working_user_id).then((user: User) => {
            MeetupService.userRegistered(working_meetup_id, user).then((_m: boolean) => {
                expect(_m).to.equal(true);
                done();
            });
        });
    });
    it("checkin", (done) => {
        MeetupService.checkin(working_meetup_id, working_user_id).then((_m: void) => {
            expect(_m).to.equal(undefined);
            done();
        });
    });
    it("accept", (done) => {
        InvitationService.find(working_invitation_id).then((i: any) => {
            expect(i).to.have.property("_id");

            MeetupService.accept(i as Invitation).then((_m: void) => {
                expect(_m).to.equal(undefined);
                done();
            });
        });
    });
    it("decline", (done) => {
        InvitationService.find(working_invitation_id).then((i: any) => {
            expect(i).to.have.property("_id");

            MeetupService.decline(i as Invitation).then((_m: void) => {
                expect(_m).to.equal(undefined);
                done();
            });
        });
    });
    it("addInvitation", (done) => {
        const user: any = {
            "isEnabled": true,
            "isAdmin": false,
            "firstName": "Rodrigo",
            "lastName": "Test",
            "username": "prueba.invitacion.meetups." + new Date().getTime() + "@gmail.com",
            "password": "asd1234"
        };
        UserService.create(user).then((u: any) => {
            const invitation: any = {
                from: working_user_id,
                to: u._id,
                meetup: working_meetup_id
            };

            InvitationService.create(invitation).then((i: any) => {
                expect(i).to.have.property("_id");

                MeetupService.addInvitation(i as Invitation).then((_m: void) => {
                    expect(_m).to.equal(undefined);
                    done();
                });
            });
        });
    });
    it("removeInvitation", (done) => {
        const user: any = {
            "isEnabled": true,
            "isAdmin": false,
            "firstName": "Rodrigo",
            "lastName": "Test",
            "username": "prueba.invitacion.meetups." + new Date().getTime() + "@gmail.com",
            "password": "asd1234"
        };
        UserService.create(user).then((u: any) => {
            const invitation: any = {
                from: working_user_id,
                to: u._id,
                meetup: working_meetup_id
            };

            InvitationService.create(invitation).then((i: any) => {
                expect(i).to.have.property("_id");

                MeetupService.addInvitation(i as Invitation).then((_m: void) => {
                    expect(_m).to.equal(undefined);

                    MeetupService.removeInvitation(i._id).then((_m: void) => {
                        expect(_m).to.equal(undefined);
                        done();
                    });
                });
            });
        });
    });
    it("getMeetupWeather", (done) => {
        MeetupService.getMeetupWeather(working_meetup_id).then((_m: any) => {
            expect(_m).to.be.a("number");
            done();
        });
    });
    it("getWeather", (done) => {
        MeetupService.getWeather(addDays(new Date(), 3)).then((_m: any) => {
            expect(_m).to.be.a("number");
            done();
        });
    });
    it("calculateBeersQuantity", (done) => {
        MeetupService.calculateBeersQuantity(working_meetup_id).then((_m: any) => {
            expect(_m).to.be.a("number");
            done();
        });
    });
    it("remove", (done) => {
        MeetupService.remove(working_meetup_id).then((_m: any) => {
            expect(_m).to.be.a("undefined");
            done();
        });
    });
});