import { expect, should } from "chai";
import * as MeetupService from "./../src/meetup/meetup.service";
import { Meetup, MeetupModel } from "./../src/meetup/meetup.class";
import * as UserService from "./../src/user/user.service";
import * as InvitationService from "./../src/invitation/invitation.service";
import { User } from "./../src/user/user.class";
import { Invitation } from "./../src/invitation/invitation.class";

let working_user_id: string = "";
let working_meetup_id: string = "";
let working_invitation_id: string = "";

function addDays(date: Date, days: number): Date {
    var result: Date = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

describe("InvitationService", () => {
    before((done) => {
        require("./test_helper");
        const user: any = {
            "isEnabled": true,
            "isAdmin": true,
            "firstName": "Rodrigo",
            "lastName": "Test",
            "username": "rodrigo.invitations." + new Date().getTime() + "@gmail.com",
            "password": "asd1234"
        };

        const meetup: any = {
            date: addDays(new Date(), 5),
            description: "meetup de prueba invitations"
        };

        UserService.create(user).then((u: any) => {
            working_user_id = u._id;
            meetup.creator = working_user_id;

            MeetupService.create(meetup).then((m: any) => {
                working_meetup_id = m._id;

                const invitation: any = {
                    from: working_user_id,
                    to: working_user_id,
                    meetup: working_meetup_id
                };

                InvitationService.create(invitation).then((i: any) => {
                    working_invitation_id = i._id;
                    done();
                });
            });
        });
    });
    it("create", (done) => {
        const meetup: any = {
            creator: working_user_id,
            date: addDays(new Date(), 5),
            description: "meetup de prueba - 2"
        };

        MeetupService.create(meetup).then((m: any) => {
            expect(m).to.have.property("_id");

            const invitation: any = {
                from: working_user_id,
                to: working_user_id,
                meetup: m._id
            };

            InvitationService.create(invitation).then((i: any) => {
                expect(i).to.have.property("_id");
                done();
            });
        });
    });
    it("findAll", (done) => {
        InvitationService.findAll().then((m: any) => {
            expect(m).to.be.a("Array");
            done();
        });
    });
    it("find", (done) => {
        InvitationService.find(working_invitation_id).then((i: any) => {
            expect(i).to.have.property("_id");
            done();
        });
    });
    it("update", (done) => {
        InvitationService.find(working_invitation_id).then((i: any) => {
            expect(i).to.have.property("_id");

            i.declined = true;
            InvitationService.update(i._id, i).then((_i: any) => {
                expect(_i).to.have.property("_id");
                expect(_i.declined).to.equal(true);

                _i.declined = false;
                InvitationService.update(_i._id, _i).then((__i: any) => {
                    expect(__i).to.have.property("_id");
                    expect(__i.declined).to.equal(false);
                    done();
                });
            });
        });
    });
    it("accept", (done) => {
        InvitationService.accept(working_invitation_id).then((i: any) => {
            expect(i).to.have.property("_id");
            done();
        });
    });
    it("decline", (done) => {
        InvitationService.decline(working_invitation_id).then((i: any) => {
            expect(i).to.have.property("_id");
            done();
        });
    });
    it("send", (done) => {
        const user: any = {
            "isEnabled": true,
            "isAdmin": false,
            "firstName": "Rodrigo",
            "lastName": "Test",
            "username": "prueba.invitacion.meetups." + new Date().getTime() + "@gmail.com",
            "password": "asd1234"
        };
        UserService.create(user).then((u: any) => {
            const invitation: any = {
                from: working_user_id,
                to: u._id,
                meetup: working_meetup_id
            };

            InvitationService.create(invitation).then((i: any) => {
                expect(i).to.have.property("_id");

                InvitationService.send(i as Invitation).then((_m: void) => {
                    expect(_m).to.equal(undefined);
                    done();
                });
            });
        });
    });
    it("remove", (done) => {
        InvitationService.remove(working_invitation_id).then((i: any) => {
            expect(i).to.be.a("undefined");
            done();
        });
    });
});