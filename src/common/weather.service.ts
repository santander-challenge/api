import * as dotenv from "dotenv";
dotenv.config();

const unirest: any = require("unirest");

/**
 * Constants
 */

const X_RAPIDAPI_KEY: string = process.env.X_RAPIDAPI_KEY as string;

const MAX_DAYS_TO_GET_WEATHER: number =
  parseInt(process.env.MAX_DAYS_TO_GET_WEATHER as string) || 5;

/**
 * Service Methods
 */

export const temp: any = async (date: Date, place: string): Promise<number> => {
  try {
    //Si la fecha ya pasó damos error
    if (date.getTime() < new Date().getTime()) {
      throw new Error("the date is in the past");
    }

    //Si supera el máximo de días que banca la API de clima le damos error
    const diffTime = date.getTime() - new Date().getTime();
    const diffDays = Math.ceil(diffTime / (1000 * 3600 * 24));

    if (diffDays > MAX_DAYS_TO_GET_WEATHER) {
      throw new Error("the date is far away for get weather");
    }

    const weather = await get_weather(place);

    if (!weather || !weather.list || !weather.list.length) {
      throw new Error("the weather is not available");
    }

    const list_by_hour = weather.list;

    //Buscamos el más cercano según fecha y hora
    let closer: number = 99999999;
    let closer_dt: number = 0;
    list_by_hour.forEach((w: any) => {
      const lapse = Math.abs(w.dt - date.getTime() / 1000);

      if (lapse < closer) {
        closer = lapse;
        closer_dt = w.dt;
      }
    });

    if (!closer_dt) {
      throw new Error("the weather is not available");
    }

    const closer_weather = list_by_hour.filter(
      (w: any) => w.dt === closer_dt
    )[0];

    //Devuelvo la temperatura, tambien podría la sensación térmica
    return closer_weather.main.temp;
  } catch (e) {
    throw e;
  }
};

const get_weather: any = async (place: string): Promise<any> => {
  try {
    return new Promise((resolve, reject) => {
      const req: any = unirest(
        "GET",
        "https://community-open-weather-map.p.rapidapi.com/forecast"
      );

      req.headers({
        "x-rapidapi-host": "community-open-weather-map.p.rapidapi.com",
        "x-rapidapi-key": X_RAPIDAPI_KEY
      });

      req.query({
        q: place,
        units: "metric"
      });
      req.end(
        async (res: any): Promise<any> => {
          if (res.error) {
            reject(res.error);
          }

          resolve(res.body);
        }
      );
    });
  } catch (e) {
    throw e;
  }
};
