import { index, prop, getModelForClass, Ref } from "@typegoose/typegoose";
const mongoose = require("mongoose");
import { User } from "./../user/user.class";
import { Meetup } from "./../meetup/meetup.class";

@index({ to: 1, meetup: 1 }, { unique: true })
export class Invitation {
  @prop({ ref: "User", refType: mongoose.Schema.Types.ObjectId, required: true })
  public from?: Ref<User>;

  @prop({ ref: "User", refType: mongoose.Schema.Types.ObjectId, required: true })
  public to?: Ref<User>;

  @prop({ ref: "Meetup", refType: mongoose.Schema.Types.ObjectId, required: true })
  public meetup?: Ref<Meetup>;

  @prop({ default: new Date() })
  public createdOn?: Date;

  @prop({ default: false })
  public declined?: boolean;

  @prop({ default: false })
  public accepted?: boolean;
}

export const InvitationModel = getModelForClass(Invitation);
