/**
 * Required External Modules and Interfaces
 */
import express, { Request, Response } from "express";
import * as InvitationService from "./invitation.service";
import { Invitation } from "./invitation.class";
import { _Res } from "../common/response.class";

/**
 * Router Definition
 */
export const invitationRouter = express.Router();

/**
 * Controller Definitions
 */

// GET invitations/
invitationRouter.get("/", async (req: Request, res: Response) => {
  try {
    const invitation: Array<Invitation> = await InvitationService.findAll();

    res.status(200).send(invitation);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// GET invitation/:id
invitationRouter.get("/:id", async (req: Request, res: Response) => {
  const id: string = req.params.id;

  try {
    const invitation: Invitation = await InvitationService.find(id);

    res.status(200).send(invitation);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// POST invitation/
invitationRouter.post("/", async (req: Request, res: Response) => {
  try {
    const invitation: Invitation = await InvitationService.create(req.body as Invitation);

    if (!invitation) {
      throw new Error("invitation not created");
    }

    await InvitationService.send(invitation);

    res.status(201).send(invitation);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// PUT invitation/
invitationRouter.put("/", async (req: Request, res: Response) => {
  try {
    const invitation: Invitation = await InvitationService.update(req.body._id, req.body as Invitation);
    res.status(201).send(invitation);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// PUT accept/
invitationRouter.put("/accept", async (req: Request, res: Response) => {
  try {
    const invitation: Invitation = await InvitationService.accept(req.body._id);
    res.status(201).send(invitation);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

invitationRouter.put("/decline", async (req: Request, res: Response) => {
  try {
    const invitation: Invitation = await InvitationService.decline(req.body._id);
    res.status(201).send(invitation);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// DELETE invitation/:id
invitationRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    const id: string = req.params.id;
    await InvitationService.remove(id);

    res.status(200).send(new _Res({ status: 200, message: "OK" }));
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});
