/**
 * Data Model Interfaces
 */

import { Invitation, InvitationModel } from "./invitation.class";
import { User } from "./../user/user.class";
import * as UserService from "./../user/user.service";
import { Meetup } from "./../meetup/meetup.class";
import * as MeetupService from "./../meetup/meetup.service";
import * as EmailService from "./../common/email.service";
import { Email } from "./../common/email.service";

/**
 * Service Methods
 */

export const findAll = async (): Promise<Array<Invitation>> => {
  return await InvitationModel.find();
};

export const find = async (id: string): Promise<Invitation> => {
  const record: Invitation = await InvitationModel.findById(id);

  if (record) {
    return record;
  }

  throw new Error("No record found");
};

export const create = async (newRecord: Invitation): Promise<Invitation> => {
  const user: User = await UserService.find(newRecord.from);

  if (!user || !user.isAdmin) {
    throw new Error("user is not admin");
  }

  const isAlreadyRegistered: boolean = await MeetupService.userRegistered(newRecord.meetup, user);

  if (isAlreadyRegistered) {
    throw new Error("user is already registered");
  }

  let invitation: Invitation;

  try {
    invitation = await InvitationModel.create(newRecord);

    if (!invitation) {
      throw new Error("can not create invitation");
    }
  } catch {
    throw new Error("user already invited");
  }

  await MeetupService.addInvitation(invitation);

  return invitation;
};

export const update = async (id: string, updatedRecord: Invitation): Promise<Invitation> => {
  const record: Invitation = await InvitationModel.findByIdAndUpdate(
    { _id: id },
    updatedRecord,
    { new: true }
  );

  if (record) {
    return record;
  }

  throw new Error("No record found to update");
};

export const remove = async (id: string): Promise<void> => {
  await MeetupService.removeInvitation(id);

  const del: any = await InvitationModel.deleteOne({ _id: id });

  if (del && del.deletedCount) {
    return;
  }

  throw new Error("No record found to delete");
};

/**
 * Custom Service Methods
 */

export const accept = async (id: string): Promise<Invitation> => {
  const invitation: Invitation = await find(id);

  if (invitation.accepted) {
    throw new Error("Already accepted");
  }

  invitation.accepted = true;
  invitation.declined = false;

  await update(id, invitation);

  await MeetupService.accept(invitation);

  return invitation;
};

export const decline = async (id: string): Promise<Invitation> => {
  const invitation: Invitation = await find(id);

  if (invitation.declined) {
    throw new Error("Already declined")
  }

  invitation.accepted = false;
  invitation.declined = true;

  await update(id, invitation);

  await MeetupService.decline(invitation);

  return invitation;
};

export const send = async (newRecord: Invitation): Promise<void> => {
  const user: User = await UserService.find(newRecord.to);

  if (!user) {
    throw new Error("user not found");
  }

  if (!user.allowNotifications) {
    return
  }

  const meetup: Meetup = await MeetupService.find(newRecord.meetup);

  if (!meetup) {
    throw new Error("meetup not found");
  }

  //Envio por mail por ahora.
  await EmailService.sendEmail(
    new Email({
      to: user.username,
      subject: `Invitación a la meetup: ${meetup.description}`,
      html: `Invitación a la meetup: ${meetup.description} el día ${meetup.date}`
    })
  );

  return;
};
