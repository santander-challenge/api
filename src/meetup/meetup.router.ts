/**
 * Required External Modules and Interfaces
 */
import express, { Request, Response } from "express";
import * as MeetupService from "./meetup.service";
import { Meetup } from "./meetup.class";
import { _Res } from "../common/response.class";

/**
 * Router Definition
 */
export const meetupRouter = express.Router();

/**
 * Controller Definitions
 */

// GET meetups/
meetupRouter.get("/", async (req: Request, res: Response) => {
  try {
    const meetup: Array<Meetup> = await MeetupService.findAll();

    res.status(200).send(meetup);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// GET beers/:id
meetupRouter.get("/:id/beers", async (req: Request, res: Response) => {
  const id: string = req.params.id;

  try {
    const beers: number = await MeetupService.calculateBeersQuantity(id);

    res.status(200).send({ beers: beers });
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// GET weather/:id
meetupRouter.get("/:id/weather", async (req: Request, res: Response) => {
  const id: string = req.params.id;

  try {
    const temp: number = await MeetupService.getMeetupWeather(id);
    res.status(200).send({ temp: temp });
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// GET meetup/:id
meetupRouter.get("/:id/populated", async (req: Request, res: Response) => {
  const id: string = req.params.id;

  try {
    const meetup: Meetup = await MeetupService.findPopulated(id);

    res.status(200).send(meetup);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// GET meetup/:id
meetupRouter.get("/:id", async (req: Request, res: Response) => {
  const id: string = req.params.id;

  try {
    const meetup: Meetup = await MeetupService.find(id);

    res.status(200).send(meetup);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// POST meetup/
meetupRouter.post("/", async (req: Request, res: Response) => {
  try {
    const meetup: Meetup = await MeetupService.create(req.body as Meetup);
    res.status(201).send(meetup);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// PUT meetup/
meetupRouter.put("/", async (req: Request, res: Response) => {
  try {
    const meetup: Meetup = await MeetupService.update(req.body._id, req.body as Meetup);
    res.status(201).send(meetup);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// PUT meetup/:id/checkin
meetupRouter.put("/:id/checkin", async (req: Request, res: Response) => {
  try {
    await MeetupService.checkin(req.params.id as string, req.body.userId as string);
    res.status(201).send(new _Res({ status: 201, message: "OK" }));
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// PUT meetup/:id/register
meetupRouter.put("/:id/register", async (req: Request, res: Response) => {
  try {
    await MeetupService.register(req.params.id as string, req.body.userId as string);
    res.status(201).send(new _Res({ status: 201, message: "OK" }));
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// DELETE meetup/:id
meetupRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    const id: string = req.params.id;
    await MeetupService.remove(id);

    res.status(200).send(new _Res({ status: 200, message: "OK" }));
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});
