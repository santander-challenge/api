import { prop, getModelForClass, Ref } from "@typegoose/typegoose";
const mongoose = require("mongoose");
import { User } from "./../user/user.class";
import { Invitation } from "./../invitation/invitation.class";

export class Meetup {
  @prop({ required: true })
  public description?: string;

  @prop({ ref: "User", refType: mongoose.Schema.Types.ObjectId, required: true })
  public creator?: Ref<User>;

  @prop({ ref: "Invitation", refType: [mongoose.Schema.Types.ObjectId] })
  public invitations?: Ref<Invitation>;

  @prop({ ref: "User", refType: [mongoose.Schema.Types.ObjectId] })
  public registered?: Ref<User>;

  @prop({ ref: "User", refType: [mongoose.Schema.Types.ObjectId] })
  public checkins?: Ref<User>;

  @prop({ required: true, default: new Date() })
  public date?: Date;

  @prop({ default: new Date() })
  public createdOn?: Date;
}

export const MeetupModel = getModelForClass(Meetup);
