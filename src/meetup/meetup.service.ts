/**
 * Data Model Interfaces
 */

import { Meetup, MeetupModel } from "./meetup.class";
import { Invitation } from "./../invitation/invitation.class";
import { User } from "./../user/user.class";
import { Email } from "./../common/email.service";
import * as UserService from "./../user/user.service";
import * as WeatherService from "./../common/weather.service";
import * as EmailService from "./../common/email.service";

/**
 * Service Methods
 */

export const findAll = async (): Promise<Array<Meetup>> => {
  return await MeetupModel.find();
};

export const find = async (id: string): Promise<Meetup> => {
  const record: Meetup = await MeetupModel.findById(id);

  if (record) {
    return record;
  }

  throw new Error("No record found");
};

export const create = async (newRecord: Meetup): Promise<Meetup> => {
  const user: User = await UserService.find(newRecord.creator);

  if (!user || !user.isAdmin) {
    throw new Error("user is not admin");
  }

  return await MeetupModel.create(newRecord);
};

export const update = async (
  id: string,
  updatedRecord: Meetup
): Promise<Meetup> => {
  const record: Meetup = await MeetupModel.findByIdAndUpdate(
    { _id: id },
    updatedRecord,
    { new: true }
  );

  if (record) {
    return record;
  }

  throw new Error("No record found to update");
};

export const remove = async (id: string): Promise<void> => {
  const del: any = await MeetupModel.deleteOne({ _id: id });

  if (del && del.deletedCount) {
    return;
  }

  throw new Error("No record found to delete");
};

/**
 * Custom Service Methods
 */

export const userRegistered = async (id: string, user: User): Promise<boolean> => {
  const meetup: Meetup = await MeetupModel.findOne(
    {
      _id: id,
      registered: user
    }
  );

  if (meetup) {
    return true;
  }

  return false;
};

export const findPopulated = async (id: string): Promise<Meetup> => {
  const record: Meetup = await MeetupModel.findById(id)
    .populate()
    .populate({ path: "invitations", model: "Invitation" })
    .populate({ path: "registered", model: "User" })
    .populate({ path: "checkins", model: "User" });

  if (record) {
    return record;
  }

  throw new Error("No record found");
};

export const checkin = async (id: string, userId: string): Promise<void> => {
  const result: any = await MeetupModel.updateOne(
    {
      _id: id,
      registered: userId,
      checkins: { $ne: userId }
    },
    {
      $push: {
        checkins: userId
      }
    }
  );

  if (result && result.nModified) {
    return;
  }

  throw new Error("error doing checkin");
};

export const register = async (id: string, userId: string): Promise<void> => {
  const result: any = await MeetupModel.updateOne(
    {
      _id: id,
      registered: { $ne: userId }
    },
    {
      $push: {
        registered: userId
      }
    }
  );

  if (result && result.nModified) {
    return;
  }

  throw new Error("error doing checkin");
};

export const accept = async (invitation: Invitation): Promise<void> => {
  const result: any = await MeetupModel.updateOne(
    { _id: invitation.meetup },
    {
      $push: {
        registered: invitation.to
      }
    }
  );

  if (!result || !result.nModified) {
    throw new Error("error accepting invitation");
  }

  const meetup: Meetup = await find(invitation.meetup);
  const user: User = await UserService.find(invitation.to);
  const admin: User = await UserService.find(invitation.from);

  //Envio por mail por ahora.
  EmailService.sendEmail(
    new Email({
      to: admin.username,
      subject: `Organización meetup: ${meetup.description}`,
      html: `El usuario ${user.username} aceptó la invitación a la meetup: ${meetup.description} el día ${meetup.date}`
    })
  );

  return;
};

export const decline = async (invitation: Invitation): Promise<void> => {
  await MeetupModel.updateOne(
    { _id: invitation.meetup },
    {
      $pull: {
        registered: invitation.to
      }
    }
  );

  const meetup: Meetup = await find(invitation.meetup);
  const user: User = await UserService.find(invitation.to);
  const admin: User = await UserService.find(invitation.from);

  //Envio por mail por ahora.
  EmailService.sendEmail(
    new Email({
      to: admin.username,
      subject: `Organización meetup: ${meetup.description}`,
      html: `El usuario ${user.username} rechazó la invitación a la meetup: ${meetup.description} el día ${meetup.date}`
    })
  );

  return;
};

export const addInvitation = async (invitation: Invitation): Promise<void> => {

  const result: any = await MeetupModel.updateOne(
    { _id: invitation.meetup },
    {
      $push: {
        invitations: invitation
      }
    }
  );

  if (result && result.nModified) {
    return;
  }

  throw new Error("error adding invitation");
};

export const removeInvitation = async (invitationId: string): Promise<void> => {
  const result: any = await MeetupModel.updateOne(
    { invitations: invitationId },
    {
      $pull: {
        invitations: invitationId
      }
    }
  );

  if (result && result.nModified) {
    return;
  }

  throw new Error("error removing invitation");
};

export const getMeetupWeather = async (id: string): Promise<number> => {
  const meetup: Meetup = await find(id);

  if (!meetup) {
    throw new Error("meetup not found");
  }

  return await getWeather(meetup.date as Date);
};

export const getWeather = async (date: Date): Promise<number> => {
  //Suponemos que todas las meetups son en CABA.
  return await WeatherService.temp(date, "Buenos Aires, AR");
};

export const calculateBeersQuantity = async (id: string): Promise<number> => {
  const meetup: Meetup = await find(id);

  if (!meetup) {
    throw new Error("meetup not found");
  }

  if (!meetup.registered) {
    throw new Error("meetup has not users registered");
  }

  const temp: number = await getWeather(meetup.date as Date);

  const beer_index = getBeersIndex(temp);

  //console.log("beer_index", beer_index);

  let beers_quantity = beer_index * meetup.registered.length;

  while (beers_quantity % 6 != 0) {
    beers_quantity++;
  }

  return beers_quantity;
};

const getBeersIndex = (temp: number) => {
  if (temp <= 20 && temp <= 24) {
    return 1;
  }
  if (temp < 20) {
    return 0.75;
  }

  return 2;
};
