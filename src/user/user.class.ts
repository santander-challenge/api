import { prop, getModelForClass } from "@typegoose/typegoose";

export class AccessToken {
  @prop({ required: true, trim: true })
  public access_token?: string;

  @prop({ required: true })
  public expires_in?: number;

  @prop({ required: true, trim: true })
  public token_type?: string;

  @prop({ required: true })
  public createdOn?: Date;

  public isEnabled(): boolean {
    let date: Date = this.createdOn || new Date();
    if (!date) {
      date = new Date();
    }
    return ((new Date().getTime() - date.getTime()) / 1000) < ((this.expires_in || 10800) - 10800);
  }

  constructor(data: any) {
    this.access_token = data.access_token || "";
    this.expires_in = data.expires_in || 0;
    this.token_type = data.token_type || "";
    this.createdOn = data.createdOn || new Date();
  }
}

export class User {
  @prop({ required: true, trim: true })
  public firstName?: string;

  @prop({ required: true, trim: true })
  public lastName?: string;

  @prop({ required: true, index: true, unique: true, lowercase: true, trim: true })
  public username?: string;

  @prop({ required: true, trim: true })
  public password?: string;

  @prop({ default: false })
  public isEnabled?: boolean;

  @prop({ default: false })
  public isAdmin?: boolean;

  @prop({ default: false })
  public allowNotifications?: boolean;

  @prop({ default: new Date() })
  public createdOn?: Date;

  @prop()
  public accessToken?: AccessToken;
}

export const UserModel = getModelForClass(User);
