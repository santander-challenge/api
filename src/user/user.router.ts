/**
 * Required External Modules and Interfaces
 */
import express, { Request, Response } from "express";
import * as UserService from "./user.service";
import { User } from "./user.class";
import { _Res } from "../common/response.class";

/**
 * Router Definition
 */
export const userRouter = express.Router();

/**
 * Controller Definitions
 */

// GET users/
userRouter.get("/", async (req: Request, res: Response) => {
  try {
    const users: Array<User> = await UserService.findAll();

    res.status(200).send(users);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// GET users/:id
userRouter.get("/:id", async (req: Request, res: Response) => {
  const id: string = req.params.id;

  try {
    const user: User = await UserService.find(id);

    res.status(200).send(user);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

userRouter.post("/login", async (req: Request, res: Response) => {
  const username: string = req.body.username;
  const password: string = req.body.password;

  try {
    const user: User = await UserService.login(username, password);

    res.status(200).send(user);
  } catch (e) {
    res.status(404).send(new _Res({ status: 404, message: e.message, request: req.params, body: req.body }));
  }
});

// POST users/
userRouter.post("/create", async (req: Request, res: Response) => {
  try {
    const user: User = await UserService.create(req.body as User);
    res.status(201).send(user);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// PUT users/
userRouter.put("/", async (req: Request, res: Response) => {
  try {
    const user: User = await UserService.update(req.body._id, req.body as User);
    res.status(201).send(user);
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});

// DELETE users/:id
userRouter.delete("/:id", async (req: Request, res: Response) => {
  try {
    const id: string = req.params.id;
    await UserService.remove(id);

    res.status(200).send(new _Res({ status: 200, message: "OK" }));
  } catch (e) {
    res.status(500).send(new _Res({ status: 500, message: e.message, request: req.params, body: req.body }));
  }
});
