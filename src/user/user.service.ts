/**
 * Data Model Interfaces
 */

import { User, UserModel, AccessToken } from "./user.class";

/**
 * Service Methods
 */

export const findAll = async (): Promise<Array<User>> => {
  return await UserModel.find();
};

export const find = async (id: string): Promise<User> => {
  const record: User = await UserModel.findById(id);

  if (record) {
    return record;
  }

  throw new Error("No record found");
};

export const create = async (newRecord: User): Promise<User> => {
  newRecord.accessToken = await createAccessToken();

  return await UserModel.create(newRecord);
};

export const update = async (id: string, updatedRecord: User): Promise<User> => {
  const record: User = await UserModel.findByIdAndUpdate(
    { _id: id },
    updatedRecord,
    { new: true }
  );

  if (record) {
    return record;
  }

  throw new Error("No record found to update");
};

export const remove = async (id: string): Promise<void> => {
  const del: any = await UserModel.deleteOne({ _id: id });

  if (del && del.deletedCount) {
    return;
  }

  throw new Error("No record found to delete");
};


/**
 * Custom Service Methods
 */
const createAccessToken = async (): Promise<AccessToken> => {
  return new Promise<AccessToken>((resolve, reject) => {
    const unirest: any = require("unirest");
    const AUTH0_DOMAIN: string = process.env.AUTH0_DOMAIN as string;
    const AUTH0_AUDIENCE: string = process.env.AUTH0_AUDIENCE as string;
    const AUTH0_CLIENT_ID: string = process.env.AUTH0_CLIENT_ID as string;
    const AUTH0_CLIENT_SECRET: string = process.env.AUTH0_CLIENT_SECRET as string;

    const req: any = unirest(
      "POST",
      `${AUTH0_DOMAIN}oauth/token`
    );

    req.headers({
      "content-type": "application/json"
    });

    req.send({
      client_id: AUTH0_CLIENT_ID,
      client_secret: AUTH0_CLIENT_SECRET,
      audience: AUTH0_AUDIENCE,
      grant_type: "client_credentials"
    });

    req.end(
      async (res: any): Promise<any> => {
        if (res.error) {
          reject(res.error);
        }

        const token: AccessToken = new AccessToken(res.body);
        token.createdOn = new Date();
        resolve(token);
      }
    );
  });
};

export const login = async (username: string, password: string): Promise<User> => {
  const record: User = await UserModel.findOne({
    username: username,
    password: password
  });

  if (record) {
    if (!record.accessToken || !record.accessToken.isEnabled()) {
      record.accessToken = await createAccessToken();
      await update((record as any)._id, record);
    }

    return record;
  }

  throw new Error("No record found");
};