import jwt from "express-jwt";
import jwksRsa from "jwks-rsa";
import * as dotenv from "dotenv";

dotenv.config();

const AUTH0_DOMAIN: string = process.env.AUTH0_DOMAIN as string;
const AUTH0_AUDIENCE: string = process.env.AUTH0_AUDIENCE as string;

export const jwtCheck: any = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: AUTH0_DOMAIN + ".well-known/jwks.json"
    }),
    audience: AUTH0_AUDIENCE,
    issuer: AUTH0_DOMAIN,
    algorithms: ["RS256"]
}).unless({
    path: [
        "/user/login",
        "/user/create"
    ]
});